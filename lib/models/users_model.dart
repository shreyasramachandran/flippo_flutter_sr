class User {
  final String id;
  final String followerUserId;
  final String followingUserId;
  final String userFullName;
  final String userCoverPictureId;
  final String userCoverPictureUrl;

  User({
    this.id,
    this.followerUserId,
    this.followingUserId,
    this.userFullName,
    this.userCoverPictureId,
    this.userCoverPictureUrl,
  });

  factory User.fromJson(Map<String, dynamic> map) {

    return User(
        id: map['id'],
        followerUserId: map['followerUserId'],
        followingUserId: map['followingUserId'],
        userFullName: map['userDetails']['fullName'],
        userCoverPictureId: map['userDetails']['coverPicture']['id'],
        userCoverPictureUrl: map['userDetails']['coverPicture']['url'],
    );
  }
}