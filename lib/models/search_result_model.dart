import 'package:flutterlistingapp/models/users_model.dart';

class SearchResult{
  List<User> users;

  SearchResult({
    this.users,
  });
}