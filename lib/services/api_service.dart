import 'dart:convert';
import 'dart:io';
import 'package:flutterlistingapp/models/search_result_model.dart';
import 'package:flutterlistingapp/models/users_model.dart';
import 'package:http/http.dart' as http;

class APIService {
  APIService._instantiate();

  static final APIService instance = APIService._instantiate();
  Map <String,String> _baseUrl = {'followers': 'https://flippo-staging.herokuapp.com/v1/followers?userId=',
                                  'following': 'https://flippo-staging.herokuapp.com/v1/following?userId='};
  String _url = '';
//  final String _token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiVVNFUiIsInN0YXR1cyI6MSwiYXV0aG9yIjoiVVNFUiIsImlkIjoiNWVjN2JiN2ZmMmIxNWYwNjgwOTE5ZWI1IiwiZmJVc2VySWQiOiIyNzg4MDcyNjQ3OTc0ODkxIiwiZnVsbE5hbWUiOiJEZWVwYW5rIEFnYXJ3YWwiLCJlbWFpbCI6ImRlZXBhbms0MTFAZ21haWwuY29tIiwiY292ZXJQaWN0dXJlIjp7InVybCI6Imh0dHBzOi8vZ3JhcGguZmFjZWJvb2suY29tLzI3ODgwNzI2NDc5NzQ4OTEvcGljdHVyZT93aWR0aD00MDAifSwiY3JlYXRlZEF0IjoiMjAyMC0wNS0yMlQxMTo0NjowNy44OTBaIiwidXBkYXRlZEF0IjoiMjAyMC0wNS0zMVQxMjo1Njo1Mi41NTdaIiwiaWF0IjoxNTkwOTI5ODEyLCJleHAiOjE2MjI0NjU4MTJ9.NTpP7GhtidHEMY3A_XC-S6xFN-GICzPqRsTOEhO0Zdc';
  int _nextPageToken = 1;

  Future<List<User>> fetchUsersSearchResult(String state,String userId,{bool stateChange = false , int limit = 20, String lastId = ''}) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
//      HttpHeaders.authorizationHeader: 'Bearer $_token'
    };

    if(stateChange){
      _nextPageToken = 1;
    }

    if(state == 'following'){
      _url = _baseUrl[state] + userId;
    }
    else{
      if(lastId != '') {
        _url = _baseUrl[state] + userId + '&limit=' + '$limit' + '&lastId=' +
            lastId;
      }
      else{
        _url = _baseUrl[state] + userId + '&limit=' + '$limit';
      }
    }

    var response = await http.get(_url, headers: headers);

    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      List<dynamic> usersJson = data['data'];

      List<User> users = [];
      usersJson.forEach(
            (json) => users.add(
          User.fromJson(json),
        ),
      );
      return users;
    } else {
      throw json.decode(response.body)['error']['message'];
    }

  }
}
