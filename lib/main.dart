import 'package:flutter/material.dart';
import 'package:flutterlistingapp/screens/home/home_screen.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter YouTube API',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.red,
      ),
      home: HomeScreen(userId: '5ec7bb7ff2b15f0680919eb5',fullName: 'shreyaZ',followersCount: 150, followingCount: 400),
    );
  }
}
