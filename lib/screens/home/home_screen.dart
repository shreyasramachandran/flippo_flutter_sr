import 'package:flutter/material.dart';
import 'package:flutterlistingapp/models/search_result_model.dart';
import 'package:flutterlistingapp/models/users_model.dart';
import 'package:flutterlistingapp/services/api_service.dart';

class HomeScreen extends StatefulWidget {
  final String userId;
  final String fullName;
  final int followersCount;
  final int followingCount;
  HomeScreen({this.userId,this.fullName,this.followersCount,this.followingCount});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  SearchResult _result;
  TabController _tabControllerTop;
  bool _isLoading = false;

  String lookingFor = "followers";
  String lastId = '';
  int curState = 0;
  int limit = 20;
  bool endFlag = false;
  // 0 for followers and 1 for following
  @override
  void initState() {
    super.initState();
    _tabControllerTop = TabController(initialIndex: 0, length: 2, vsync: this);
    _initChannel();
  }

  _initChannel() async {
    SearchResult result = SearchResult();
    List<User> moreUsers = await APIService.instance.fetchUsersSearchResult(lookingFor,widget.userId,limit: limit,lastId: lastId);
    setState(() {
      _result = result;
      _result.users = moreUsers;
      lastId = moreUsers.last.id;
    });
    endFlag = _endLimitCheck(_result.users.length);
  }

  _changeState(int futureState) async{
    if(curState != futureState){
      String futureLookingFor = curState == 0 ? "followers" : "following";
      SearchResult result = SearchResult();
      List<User> moreUsers = await APIService.instance.fetchUsersSearchResult(lookingFor,widget.userId,stateChange:true,limit: limit,lastId: lastId);
      setState(() {
        _result = result;
        _result.users = moreUsers;
        lookingFor = futureLookingFor;
        curState = futureState;
        lastId = moreUsers.last.id;
      });
      endFlag = _endLimitCheck(_result.users.length);
    }
  }

  _loadMoreUsers() async {
    _isLoading = true;
    List<User> moreUsers = await APIService.instance.fetchUsersSearchResult(lookingFor,widget.userId,limit: limit,lastId: lastId);
    List<User> allUsers = _result.users..addAll(moreUsers);
    setState(() {
      _result.users = allUsers;
      lastId = moreUsers.last.id;
    });
    _isLoading = false;
    endFlag = _endLimitCheck(_result.users.length);
  }

  _endLimitCheck(int usersLength){
    if(usersLength < limit){
      return true;
    }
    else{
      return false;
    }
  }

  _buildUser(User user) {
    return GestureDetector(
      onTap: () => {},
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
        child:  Row(
          children: <Widget>[
            Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: NetworkImage(user.userCoverPictureUrl),
                    fit: BoxFit.fill
                ),
              ),
            ),
            SizedBox(width: 30,),
            Column(
              children: <Widget>[
                Text(user.userFullName,
                    style: TextStyle(color: Colors.white)),
              ],
            )
          ],
        )
      ),
    );
  }

  _listingWidget() {
    return Container(
      color: Colors.black,
      child: _result != null
          ? NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollDetails) {
                if (!_isLoading &&
                    !endFlag &&
                    scrollDetails.metrics.pixels ==
                        scrollDetails.metrics.maxScrollExtent) {
                  _loadMoreUsers();
                }
                return false;
              },
              child: ListView.builder(
                itemCount: 1 + _result.users.length,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return Column(
                      children: <Widget>[
                        _topMostTab(widget.followersCount,widget.followingCount),
                        SizedBox(height: 5,)
                      ],
                    );
                  }
                  User user = _result.users[index - 1];
                  return _buildUser(user);
                },
              ),
            )
          : Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).primaryColor, // Red
                ),
              ),
            ),
    );
  }

  _topMostTab(int followersCount,int followingCount) {
    return Container(
      color: Colors.black12,
      child: DefaultTabController(
        length: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TabBar(
              controller: _tabControllerTop,
              indicatorColor: Colors.white,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.grey.withOpacity(1),
              labelPadding: EdgeInsets.symmetric(horizontal: 20.0),
              isScrollable: false,
              onTap: (index) {
                _changeState(index);
              },
              tabs: <Widget>[
                Tab(
                    child: Text(
                  '$followersCount Followers',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                )),
                Tab(
                  child: Text(
                    '$followingCount Following',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ]),
        ),
      ),
    );
  }

  _nameBar(){
    return AppBar(
      backgroundColor: Colors.black54,
      leading: BackButton(
        color: Colors.black,
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text(widget.fullName),
      centerTitle: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: <Widget>[
          _nameBar(),
          Expanded(child: _listingWidget()),
        ]),
      ),
    );
  }
}
